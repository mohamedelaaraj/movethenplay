import {
  IonButton,
  IonContent,
  IonHeader,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import { Swiper, SwiperSlide, useSwiper } from "swiper/react";
import "swiper/css";

const RepportSlider: React.FC = () => {
  return (
    <Swiper>
      <SwiperSlide>
        <IonText className="ion-margin">
          <div className="ion-text-center">
            <h4>
              Réduire la sidentarité chez les enfants, les jeunes et les adults
            </h4>
          </div>
        </IonText>
      </SwiperSlide>
      <SwiperSlide>
        <IonText className="ion-margin">
          <div className="ion-text-center">
            <h4>Maximiser votre utilisation de la gamification</h4>
          </div>
        </IonText>
      </SwiperSlide>
      <SwiperSlide>
        <IonText className="ion-margin">
          <div className="ion-text-center">
            <h4>Profitez d'une expérience formidable avec Bouge pour jouer</h4>
          </div>
        </IonText>
      </SwiperSlide>
    </Swiper>
  );
};

export default RepportSlider;
