import { IonButton, IonContent, IonHeader, IonPage, IonText, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react';
import "swiper/css";
import './Intro.css'
import Intro1Svg from '../assets/intro/Intro1Svg.svg'
import Intro2Svg from '../assets/intro/Intro2Svg.svg'
import Intro3Svg from '../assets/intro/Intro3Svg.svg'


interface ContainerProps {
    onFinish:() => void;
}

const SwiperButtonNext = ({children}: any) => {
    const swiper = useSwiper();
    return <IonButton 
    shape="round" 
    color={"secondary"}  
    expand="full" 
    className='ion-margin-top' 
    onClick={()=>swiper.slideNext()}>{ children }</IonButton>
}
const Intro: React.FC<ContainerProps> = ({ onFinish }) => {
   

    return (

        <Swiper>
        <SwiperSlide>
                <img src={Intro1Svg} alt="Intro 1" />
        <IonText className='ion-margin'>
                    <div className='ion-text-center'>
                    <h4>Réduire la sidentarité chez les enfants, les jeunes et les adults</h4>
                    </div>
        </IonText>
        <SwiperButtonNext >Suivant</SwiperButtonNext>
                

        </SwiperSlide>
        <SwiperSlide>
            <img src={Intro2Svg} alt="Intro 2" />
         <IonText className='ion-margin'>
                    <div className='ion-text-center'>
                    <h4>Maximiser votre utilisation de la gamification</h4>
                    </div>
        </IonText>
        <SwiperButtonNext >Suivant</SwiperButtonNext>
        </SwiperSlide>
        <SwiperSlide>
            <img src={Intro3Svg} alt="Intro 3" />
        <IonText className='ion-margin'>
                    <div className='ion-text-center'>
                    <h4>Profitez d'une expérience formidable avec Bouge pour jouer</h4>
                    </div>
                </IonText>
         <IonButton 
         shape="round" 
         color={"secondary"} 
         type="button" 
         expand="block" 
         className='ion-margin-top'
         onClick={()=>onFinish()}
         >
            Terminer l'intro
        </IonButton>

            </SwiperSlide>
        </Swiper>
       
    );
};

export default Intro;