import { IonSegmentCustomEvent } from "@ionic/core";
import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonPage,
  IonSegment,
  IonSegmentButton,
  IonTitle,
  IonToolbar,
  SegmentChangeEventDetail,
} from "@ionic/react";
import { backspace, notificationsOutline } from "ionicons/icons";
import React from "react";
import RepportSlider from "../components/RepportSlider";

const Repport: React.FC = () => {
  function handlesegmentchange(
    e: IonSegmentCustomEvent<SegmentChangeEventDetail>
  ): void {
    throw new Error("Function not implemented.");
  }

  return (
    <IonPage>
      <IonHeader class="ion-no-border">
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/app/settings/summary">
              <IonIcon icon={backspace} title="none" />
            </IonBackButton>
          </IonButtons>
          <IonButtons slot="end">
            <IonButton color="dark" id="open-modal" expand="block">
              <IonIcon
                slot="icon-only"
                ios={notificationsOutline}
                md={notificationsOutline}
              ></IonIcon>
            </IonButton>
          </IonButtons>

          <IonTitle>Rapport de consommation</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonSegment value="buttons" onIonChange={(e) => handlesegmentchange(e)}>
          <IonSegmentButton value="default">
            <IonLabel>Moi</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="2">
            <IonLabel>George</IonLabel>
          </IonSegmentButton>
          <IonSegmentButton value="3">
            <IonLabel>Luna</IonLabel>
          </IonSegmentButton>
        </IonSegment>

        <RepportSlider />
      </IonContent>
    </IonPage>
  );
};

export default Repport;
