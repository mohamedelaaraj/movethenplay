import {
  IonAccordion,
  IonAccordionGroup,
  IonAvatar,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCheckbox,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonMenuButton,
  IonModal,
  IonPage,
  IonRow,
  IonTitle,
  IonToggle,
  IonToolbar,
} from "@ionic/react";
import React, { useEffect, useRef, useState } from "react";
import "../Tabs/Summary.css";
import {
  appsOutline,
  chevronForwardOutline,
  closeCircleOutline,
  gift,
  notificationsOutline,
} from "ionicons/icons";
import "react-circular-progressbar/dist/styles.css";
import {
  CircularProgressbar,
  CircularProgressbarWithChildren,
  buildStyles,
} from "react-circular-progressbar";
import { Flat, Heat } from "@alptugidin/react-circular-progress-bar";

const Summary: React.FC = () => {
  const modal = useRef<HTMLIonModalElement>(null);
  const page = useRef(undefined);

  const [canDismiss, setCanDismiss] = useState(false);
  const [presentingElement, setPresentingElement] = useState<
    HTMLElement | undefined
  >(undefined);

  useEffect(() => {
    setPresentingElement(page.current);
  }, []);

  function dismiss() {
    modal.current?.dismiss();
  }
  return (
    <IonPage ref={page}>
      <IonHeader class="ion-no-border">
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton>
              <IonIcon icon={appsOutline} color="primary"></IonIcon>
            </IonMenuButton>
          </IonButtons>
          <IonTitle></IonTitle>
          <IonButtons slot="end">
            <IonButton color="dark" id="open-modal" expand="block">
              <IonIcon
                slot="icon-only"
                ios={notificationsOutline}
                md={notificationsOutline}
              ></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent className="">
        {/* Notification model */}

        <IonModal
          ref={modal}
          trigger="open-modal"
          canDismiss={canDismiss}
          presentingElement={presentingElement}
        >
          <IonHeader>
            <IonToolbar>
              <IonTitle>Notification</IonTitle>
              <IonButtons slot="end">
                <IonButton onClick={() => dismiss()}>
                  <IonIcon icon={closeCircleOutline} />
                </IonButton>
              </IonButtons>
            </IonToolbar>
          </IonHeader>
          <IonContent>
            <p className="ion-padding-horizontal">
              You must accept the terms and conditions to close this modal.
            </p>
            <IonItem>
              <IonCheckbox
                id="terms"
                checked={canDismiss}
                onIonChange={(ev) => {
                  setCanDismiss(ev.detail.checked);
                }}
              >
                <div className="ion-text-wrap">
                  Do you accept the terms and conditions?
                </div>
              </IonCheckbox>
            </IonItem>
          </IonContent>
        </IonModal>

        <IonCard id="home-card" className="ion-no-padding">
          <IonCardContent>
            <IonItem lines="none">
              <IonAvatar className="item">
                <img className="item-avatar" src="/public/user1.png" />
              </IonAvatar>
              <IonLabel className="ion-margin-start">
                Bienvenue
                <p>Mohamed EL AARAJ</p>
              </IonLabel>

              <div style={{ width: 90 }}>
                <CircularProgressbarWithChildren
                  value={50}
                  text={"00:48"}
                  className=" ion-text-center circle-text-bold"
                  styles={buildStyles({
                    textSize: "17px",
                    // Colors
                    pathColor: `#F9B234`,
                    textColor: "#000000",
                    backgroundColor: "transparent",
                    pathTransitionDuration: 0.5,
                  })}
                >
                  <br />
                  <p style={{ fontSize: 8, marginTop: 6 }}>Temps de jeu</p>
                </CircularProgressbarWithChildren>
              </div>
            </IonItem>
          </IonCardContent>
        </IonCard>

        <IonCard id="home-card" className="ion-no-padding">
          <IonCardContent className="ion-no-padding ">
            <IonItem lines="none" className="ion-text-center ion-margin  ">
              <IonGrid fixed>
                <IonRow class="ion-justify-content-center ">
                  <IonCol sizeMd="5" sizeLg="4" sizeXl="2">
                    <IonLabel className="ion-text-center">
                      <h2>Jeux vidéo</h2>
                    </IonLabel>
                    <CircularProgressbarWithChildren
                      value={70}
                      text={"1h00"}
                      className="ion-text-center  ion-margin-top"
                      styles={buildStyles({
                        textSize: "16px",
                        // Colors
                        pathColor: `#F9B234`,
                        textColor: "#000000",
                      })}
                    >
                      <div style={{ fontSize: 12, marginTop: -5 }}>
                        <br />
                        <br />
                        <br />
                        <p>sur 5h00</p>
                      </div>
                    </CircularProgressbarWithChildren>
                  </IonCol>

                  <IonCol sizeMd="5" sizeLg="4" sizeXl="2">
                    <IonLabel className="ion-text-center">
                      <h2>Sport</h2>
                    </IonLabel>
                    <CircularProgressbarWithChildren
                      value={50}
                      text={"2h30"}
                      className=" ion-text-center  ion-margin-top"
                      styles={buildStyles({
                        textSize: "16px",
                        // Colors
                        pathColor: `#45DEBF`,
                        textColor: "#000000",
                      })}
                    >
                      <div style={{ fontSize: 12, marginTop: -5 }}>
                        <br />
                        <br />
                        <br />
                        <p>sur 7h00</p>
                      </div>
                    </CircularProgressbarWithChildren>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonItem>

            <IonItem lines="none">
              <IonLabel className=" ion-text-center">
                4h00 de jeux vidéo disponibles
                {/* <IonProgressBar className='ion-margin-top ion-margin-end' value={progress}></IonProgressBar> */}
              </IonLabel>
            </IonItem>

            <IonButton
              shape="round"
              slot="start"
              expand="block"
              color={"primary"}
              className="ion-margin"
              href="/app/settings/gifts"
            >
              Voir mes cadeaux
              <IonIcon slot="end" icon={gift} color={"light"}></IonIcon>
            </IonButton>

            <IonButton
              shape="round"
              expand="block"
              color={"light"}
              className="ion-margin"
            >
              Voir mon activité
              <IonIcon
                slot="end"
                icon={chevronForwardOutline}
                color={"dark"}
              ></IonIcon>
            </IonButton>

            <IonButton
              shape="round"
              expand="block"
              color={"light"}
              className="ion-margin"
              href="/app/settings/repport"
            >
              Rapport de consommation
              <IonIcon
                slot="end"
                icon={chevronForwardOutline}
                color={"dark"}
              ></IonIcon>
            </IonButton>
          </IonCardContent>
        </IonCard>

        {/* Other kids */}

        <IonItem lines="none">
          <IonLabel className="ion-text-center">Membre de ma famille </IonLabel>
        </IonItem>

        <IonAccordionGroup className="ion-no-padding">
          <IonAccordion value="first">
            <IonItem slot="header" className="accordion-group " color="medium">
              <IonAvatar>
                <CircularProgressbarWithChildren
                  value={75}
                  strokeWidth={8}
                  styles={buildStyles({
                    pathColor: "#F9B234",
                    trailColor: "transparent",
                  })}
                >
                  <div style={{ width: "80%" }}>
                    <CircularProgressbar
                      value={70}
                      styles={buildStyles({
                        pathColor: "#45DEBF",
                        trailColor: "transparent",
                      })}
                    ></CircularProgressbar>
                  </div>
                </CircularProgressbarWithChildren>
              </IonAvatar>
              <IonLabel className="ion-margin-start">
                Thierry <p>Enfant</p>
              </IonLabel>
            </IonItem>
            <div className="" slot="content">
              <IonCard id="home-card" className="ion-no-padding">
                <IonCardContent className="ion-no-padding">
                  <IonItem lines="none" className="ion-text-center ion-margin">
                    <IonGrid fixed>
                      <IonRow class="ion-justify-content-center">
                        <IonCol sizeMd="5" sizeLg="4" sizeXl="2">
                          <IonLabel className="ion-text-center">
                            <h2>Jeux vidéos</h2>
                          </IonLabel>
                          <CircularProgressbarWithChildren
                            value={70}
                            text={"2h"}
                            className="ion-text-center  ion-margin-top"
                            styles={buildStyles({
                              textSize: "16px",
                              // Colors
                              pathColor: `#F9B234`,
                              textColor: "#000000",
                            })}
                          ></CircularProgressbarWithChildren>
                          <div
                            className="ion-padding-top"
                            style={{ fontSize: 2, marginTop: -5 }}
                          >
                            <p>Temps restant</p>
                          </div>
                        </IonCol>

                        <IonCol sizeMd="5" sizeLg="4" sizeXl="2">
                          <IonLabel className="ion-text-center">
                            <h2>Sport</h2>
                          </IonLabel>
                          <CircularProgressbarWithChildren
                            value={50}
                            text={"3h"}
                            className=" ion-text-center  ion-margin-top"
                            styles={buildStyles({
                              textSize: "16px",
                              // Colors
                              pathColor: `#45DEBF`,
                              textColor: "#000000",
                            })}
                          ></CircularProgressbarWithChildren>
                          <div
                            className="ion-padding-top"
                            style={{ fontSize: 2, marginTop: -5 }}
                          >
                            <p>Temps hebdomadaire</p>
                          </div>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </IonItem>

                  <IonButton
                    shape="round"
                    slot="start"
                    expand="block"
                    color={"medium"}
                    className="ion-margin"
                  >
                    Voir les activités
                    <IonIcon
                      slot="end"
                      icon={chevronForwardOutline}
                      color={"light"}
                    ></IonIcon>
                  </IonButton>

                  <IonButton
                    shape="round"
                    expand="block"
                    color={"medium"}
                    className="ion-margin"
                  >
                    Rapport de consommation
                    <IonIcon
                      slot="end"
                      icon={chevronForwardOutline}
                      color={"light"}
                    ></IonIcon>
                  </IonButton>
                  <IonButton
                    shape="round"
                    expand="block"
                    color={"medium"}
                    className="ion-margin"
                  >
                    <IonToggle slot="end" checked={true}>
                      Bloquer son accés
                    </IonToggle>
                  </IonButton>
                </IonCardContent>
              </IonCard>
            </div>
          </IonAccordion>

          <IonAccordion value="second">
            <IonItem slot="header" className="accordion-group" color="medium">
              <IonAvatar>
                <CircularProgressbarWithChildren
                  value={75}
                  strokeWidth={8}
                  styles={buildStyles({
                    pathColor: "#F9B234",
                    trailColor: "transparent",
                  })}
                >
                  <div style={{ width: "80%" }}>
                    <CircularProgressbar
                      value={70}
                      styles={buildStyles({
                        pathColor: "#45DEBF",
                        trailColor: "transparent",
                      })}
                    ></CircularProgressbar>
                  </div>
                </CircularProgressbarWithChildren>
              </IonAvatar>
              <IonLabel className="ion-margin-start">
                Enfant 2<p>Enfant</p>
              </IonLabel>
            </IonItem>
            <div className="" slot="content">
              <IonCard id="home-card" className="ion-no-padding">
                <IonCardContent className="ion-no-padding">
                  <IonItem lines="none" className="ion-text-center ion-margin">
                    <IonGrid fixed>
                      <IonRow class="ion-justify-content-center">
                        <IonCol sizeMd="5" sizeLg="4" sizeXl="2">
                          <IonLabel className="ion-text-center">
                            <h2>Jeux vidéo</h2>
                          </IonLabel>
                          <CircularProgressbarWithChildren
                            value={70}
                            text={"1h00"}
                            className="ion-text-center  ion-margin-top"
                            styles={buildStyles({
                              textSize: "16px",
                              // Colors
                              pathColor: `#F9B234`,
                              textColor: "#000000",
                            })}
                          >
                            <div style={{ fontSize: 12, marginTop: -5 }}>
                              <br />
                              <br />
                              <br />
                              <p>sur 5h00</p>
                            </div>
                          </CircularProgressbarWithChildren>
                        </IonCol>

                        <IonCol sizeMd="5" sizeLg="4" sizeXl="2">
                          <IonLabel className="ion-text-center">
                            <h2>Sport</h2>
                          </IonLabel>
                          <CircularProgressbarWithChildren
                            value={50}
                            text={"2h30"}
                            className=" ion-text-center  ion-margin-top"
                            styles={buildStyles({
                              textSize: "16px",
                              // Colors
                              pathColor: `#45DEBF`,
                              textColor: "#000000",
                            })}
                          >
                            <div style={{ fontSize: 12, marginTop: -5 }}>
                              <br />
                              <br />
                              <br />
                              <p>sur 7h00</p>
                            </div>
                          </CircularProgressbarWithChildren>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </IonItem>

                  <IonItem lines="none">
                    <IonLabel className=" ion-text-center">
                      4h00 de jeux vidéo disponibles
                      {/* <IonProgressBar className=' ion-margin-top' value={progress}></IonProgressBar> */}
                    </IonLabel>
                  </IonItem>

                  <IonButton
                    shape="round"
                    expand="block"
                    color={"medium"}
                    className="ion-margin"
                  >
                    Voir les activités
                    <IonIcon
                      slot="end"
                      icon={chevronForwardOutline}
                      color={"light"}
                    ></IonIcon>
                  </IonButton>

                  <IonButton
                    shape="round"
                    expand="block"
                    color={"medium"}
                    className="ion-margin"
                  >
                    Rapport de consommation
                    <IonIcon
                      slot="end"
                      icon={chevronForwardOutline}
                      color={"light"}
                    ></IonIcon>
                  </IonButton>
                </IonCardContent>
              </IonCard>
            </div>
          </IonAccordion>

          <IonAccordion value="third">
            <IonItem slot="header" className="accordion-group" color="medium">
              <IonAvatar>
                <CircularProgressbarWithChildren
                  value={75}
                  strokeWidth={8}
                  styles={buildStyles({
                    pathColor: "#F9B234",
                    trailColor: "transparent",
                  })}
                >
                  <div style={{ width: "80%" }}>
                    <CircularProgressbar
                      value={70}
                      styles={buildStyles({
                        pathColor: "#45DEBF",
                        trailColor: "transparent",
                      })}
                    ></CircularProgressbar>
                  </div>
                </CircularProgressbarWithChildren>
              </IonAvatar>
              <IonLabel className="ion-margin-start">
                Enfant 3<p>Enfant</p>{" "}
              </IonLabel>
            </IonItem>
            <div className="" slot="content">
              <IonCard id="home-card" className="ion-no-padding">
                <IonCardContent className="ion-no-padding">
                  <IonItem lines="none" className="ion-text-center ion-margin">
                    <IonGrid fixed>
                      <IonRow class="ion-justify-content-center">
                        <IonCol sizeMd="5" sizeLg="4" sizeXl="2">
                          <IonLabel className="ion-text-center">
                            <h2>Jeux vidéo</h2>
                          </IonLabel>
                          <CircularProgressbarWithChildren
                            value={70}
                            text={"1h00"}
                            className="ion-text-center  ion-margin-top"
                            styles={buildStyles({
                              textSize: "16px",
                              // Colors
                              pathColor: `#F9B234`,
                              textColor: "#000000",
                            })}
                          >
                            <div style={{ fontSize: 12, marginTop: -5 }}>
                              <br />
                              <br />
                              <br />
                              <p>sur 5h00</p>
                            </div>
                          </CircularProgressbarWithChildren>
                        </IonCol>

                        <IonCol sizeMd="5" sizeLg="4" sizeXl="2">
                          <IonLabel className="ion-text-center">
                            <h2>Sport</h2>
                          </IonLabel>
                          <CircularProgressbarWithChildren
                            value={50}
                            text={"2h30"}
                            className=" ion-text-center  ion-margin-top"
                            styles={buildStyles({
                              textSize: "16px",
                              // Colors
                              pathColor: `#45DEBF`,
                              textColor: "#000000",
                            })}
                          >
                            <div style={{ fontSize: 12, marginTop: -5 }}>
                              <br />
                              <br />
                              <br />
                              <p>sur 7h00</p>
                            </div>
                          </CircularProgressbarWithChildren>
                        </IonCol>
                      </IonRow>
                    </IonGrid>
                  </IonItem>

                  <IonItem lines="none">
                    <IonLabel className=" ion-text-center">
                      4h00 de jeux vidéo disponibles
                      {/* <IonProgressBar className=' ion-margin-top' value={progress}></IonProgressBar> */}
                    </IonLabel>
                  </IonItem>

                  <IonButton
                    shape="round"
                    expand="block"
                    color={"medium"}
                    className="ion-margin"
                  >
                    Voir les activités
                    <IonIcon
                      slot="end"
                      icon={chevronForwardOutline}
                      color={"light"}
                    ></IonIcon>
                  </IonButton>

                  <IonButton
                    shape="round"
                    expand="block"
                    color={"medium"}
                    className="ion-margin"
                  >
                    Rapport de consommation
                    <IonIcon
                      slot="end"
                      icon={chevronForwardOutline}
                      color={"light"}
                    ></IonIcon>
                  </IonButton>
                </IonCardContent>
              </IonCard>
            </div>
          </IonAccordion>
        </IonAccordionGroup>
      </IonContent>
    </IonPage>
  );
};

export default Summary;
