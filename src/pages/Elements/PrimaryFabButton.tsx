import {
  IonContent,
  IonFab,
  IonFabButton,
  IonFabList,
  IonHeader,
  IonIcon,
  IonLabel,
  IonModal,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import {
  add,
  addCircle,
  addOutline,
  chevronUpCircle,
  colorPalette,
  document,
  gameController,
  gameControllerOutline,
  giftOutline,
  globe,
  hourglass,
  hourglassOutline,
  lockClosed,
  lockClosedOutline,
} from "ionicons/icons";
import React from "react";
import "./fabStyle.css";

const PrimaryFabButton: React.FC = () => {
  return (
    <IonFab
      vertical="bottom"
      horizontal="center"
      style={{ paddingBottom: "var(--ion-safe-area-bottom, 0)" }}
    >
      <IonFabButton>
        <IonIcon icon={addOutline}></IonIcon>
      </IonFabButton>
      <IonFabList side="top">
        <IonFabButton
          color={"danger"}
          data-desc="Entrée manuelle"
          href="/app/settings/record-manual"
        >
          <IonIcon icon={add}></IonIcon>
        </IonFabButton>
        <IonFabButton
          color={"danger"}
          data-desc="Chronomètre"
          href="/app/settings/record-chronometer"
        >
          <IonIcon icon={hourglassOutline}></IonIcon>
        </IonFabButton>
        <IonFabButton
          color={"danger"}
          data-desc="Jeux Vidéo"
          href="/app/settings/record-console"
        >
          <IonIcon icon={gameControllerOutline}>
            <IonLabel>Text1</IonLabel>
          </IonIcon>
        </IonFabButton>
        <IonFabButton
          color={"danger"}
          data-desc="Cadeau"
          href="/app/settings/add-gift"
        >
          <IonIcon icon={giftOutline}>
            <IonLabel>Text1</IonLabel>
          </IonIcon>
        </IonFabButton>

        <IonFabButton color={"danger"} data-desc="Pénalité">
          <IonIcon icon={lockClosedOutline}>
            <IonLabel>Text1</IonLabel>
          </IonIcon>
        </IonFabButton>
      </IonFabList>
      <div className="mtp-record-backdrop"></div>
    </IonFab>
  );
};

export default PrimaryFabButton;
