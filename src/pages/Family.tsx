import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';

const Family: React.FC = () => {

    return (
        <>
        <IonHeader>
          <IonToolbar>
          <IonButtons slot='start'>
                    <IonMenuButton></IonMenuButton>
                </IonButtons>
            <IonTitle>Family</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              height: '100%',
            }}
          >
            Family
          </div>
        </IonContent>
      </>
    );
};

export default Family;