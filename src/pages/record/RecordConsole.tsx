import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonLoading,
  IonModal,
  IonPage,
  IonRow,
  IonSegment,
  IonSegmentButton,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToast,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import {
  addSharp,
  backspace,
  caretForwardOutline,
  checkmarkCircleOutline,
  closeCircleOutline,
  notificationsOutline,
  pauseOutline,
  pauseSharp,
  playBackOutline,
  playForward,
  playOutline,
  squareOutline,
} from "ionicons/icons";
import React, { useEffect, useRef, useState } from "react";
import "./RecordChrono.css";
import {
  CircularProgressbar,
  CircularProgressbarWithChildren,
  buildStyles,
} from "react-circular-progressbar";

const RecordConsole: React.FC = () => {
  const [isPaused, setIsPaused] = useState(false);

  const [isOpen, setIsOpen] = useState(false);
  const [isOpen2, setIsOpen2] = useState(false);

  const handleTogglePause = () => {
    setIsPaused(!isPaused);
  };

  //Alert
  const router = useIonRouter();
  const [showAlert, setShowAlert] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const handleDeleteConfirm = () => {
    // Handle delete logic here
    setShowAlert(false);
    setIsOpen2(false);
  };

  //Ion Loading progress bar
  const [showLoading, setShowLoading] = useState(false);

  const startLoading = () => {
    setShowLoading(true);

    // Simulate a delay (e.g., fetch data or perform an operation)
    setTimeout(() => {
      setShowLoading(false);
    }, 3000);
  };
  return (
    <IonPage>
      <IonHeader class="ion-no-border">
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/app/settings/summary">
              <IonIcon icon={backspace} title="none" />
            </IonBackButton>
          </IonButtons>
          <IonTitle></IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding ion-content-centered">
        <IonLabel className="ion-text-center">
          <h1>Informations de la séance</h1>
        </IonLabel>

        <IonRow class="ion-justify-content-center ion-margin-top">
          <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
            <IonItem>
              <IonSelect
                labelPlacement="floating"
                fill="solid"
                label="Description"
                interface="popover"
                placeholder="Mohamed El Aaraj"
                multiple={true}
              >
                <IonSelectOption value="1">Choix 1</IonSelectOption>
                <IonSelectOption value="2">Choix 2</IonSelectOption>
                <IonSelectOption value="3">Choix 3</IonSelectOption>
                <IonSelectOption value="4">Choix 4</IonSelectOption>
              </IonSelect>
            </IonItem>
          </IonCol>
        </IonRow>

        <IonRow class="ion-justify-content-center ion-margin-top">
          <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
            <IonItem>
              <IonSelect
                labelPlacement="floating"
                fill="solid"
                label="Quelle console ?"
                interface="popover"
                placeholder="Console 1"
                multiple={true}
              >
                <IonSelectOption value="1">Console 1</IonSelectOption>
                <IonSelectOption value="2">Console 2</IonSelectOption>
                <IonSelectOption value="3">Console 3</IonSelectOption>
                <IonSelectOption value="4">Console 4</IonSelectOption>
              </IonSelect>
            </IonItem>
          </IonCol>
        </IonRow>

        <IonRow class="ion-justify-content-center">
          <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
            <IonButton
              shape="round"
              expand="full"
              fill="solid"
              color={"secondary"}
              size="default"
              className="ion-margin-top ion-circle-button ion-padding"
              onClick={() => setIsOpen(true)}
            >
              Commencer
            </IonButton>
          </IonCol>
        </IonRow>
      </IonContent>

      <IonModal isOpen={isOpen} trigger="open-modal">
        <IonHeader class="ion-no-border">
          <IonToolbar>
            <IonButtons slot="end">
              <IonButton onClick={() => setIsOpen(false)}>
                <IonIcon icon={closeCircleOutline} />
              </IonButton>
            </IonButtons>

            <IonTitle></IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-no-padding ion-text-center ">
          <IonRow class="ion-justify-content-center ion-margin-top">
            <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
              <IonLabel>
                <h1>Informations de la séance</h1>
              </IonLabel>
            </IonCol>
          </IonRow>

          <IonRow>
            <IonCol>
              <p>En attente du début de la session</p>
            </IonCol>
          </IonRow>

          <IonRow class="ion-justify-content-center ion-margin-top">
            <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
              <div className="ion-text-center centered-progress-container ">
                <div style={{ width: 100, height: 100 }}>
                  <CircularProgressbar
                    value={66}
                    styles={buildStyles({
                      trailColor: "#d6d6d6",
                      backgroundColor: "#467DF6",
                      pathColor: "#467DF6",
                    })}
                  />
                </div>
              </div>
            </IonCol>
          </IonRow>

          <IonRow class="ion-justify-content-center ion-margin-top ion-padding">
            <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
              <p>Allumez votre console pour commencer à jouer</p>
            </IonCol>
          </IonRow>

          <IonRow class="ion-justify-content-center ion-padding">
            <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
              {/* Additional content you want at the bottom */}
              <IonButton
                shape="round"
                expand="full"
                fill="solid"
                color={"primary"}
                size="default"
                className="ion-circle-button"
                onClick={() => setIsOpen2(true)}
              >
                Commencer
              </IonButton>
            </IonCol>
            <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
              {/* Additional content you want at the bottom */}
              <IonButton
                shape="round"
                expand="full"
                fill="outline"
                color={"danger"}
                size="default"
                className="ion-circle-button"
                onClick={() => setShowAlert(true)}
              >
                Annuler
              </IonButton>
            </IonCol>
          </IonRow>
        </IonContent>
      </IonModal>

      <IonModal isOpen={isOpen2} trigger="open-modal-2">
        <IonHeader class="ion-no-border">
          <IonToolbar>
            <IonButtons slot="end">
              <IonButton onClick={() => setIsOpen2(false)}>
                <IonIcon icon={closeCircleOutline} />
              </IonButton>
            </IonButtons>

            <IonTitle></IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-no-padding ion-text-center ">
          <IonRow class="ion-justify-content-center ion-margin-top">
            <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
              <IonLabel>
                <h1>Informations de la séance</h1>
              </IonLabel>
            </IonCol>
          </IonRow>

          <IonRow class="ion-justify-content-center ion-margin-top">
            <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
              <div className="ion-text-center centered-progress-container ">
                <CircularProgressbarWithChildren
                  value={70}
                  text={"00:25:12"}
                  className=" ion-text-center circle-text-bold"
                  styles={buildStyles({
                    textSize: "18px",
                    // Colors
                    pathColor: `#45DEBF`,
                    textColor: "#000000",
                    backgroundColor: "transparent",
                    pathTransitionDuration: 0.4,
                  })}
                ></CircularProgressbarWithChildren>
              </div>
            </IonCol>
          </IonRow>

          <IonRow class="ion-justify-content-center ion-margin-top">
            <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
              {/* Additional content you want at the bottom */}
              <IonButton
                shape="round"
                fill="solid"
                color={"primary"}
                size="default"
                className="ion-circle-button"
                onClick={handleTogglePause}
              >
                <IonIcon
                  src={isPaused ? pauseOutline : caretForwardOutline}
                ></IonIcon>
              </IonButton>
            </IonCol>

            <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
              {/* Additional content you want at the bottom */}
              <IonButton
                shape="round"
                fill="solid"
                color={"danger"}
                size="default"
                expand="full"
                className="ion-circle-button ion-padding"
                onClick={() => setIsOpen2(false)}
              >
                {" "}
                <IonLabel className="">Terminer</IonLabel>
              </IonButton>
            </IonCol>
          </IonRow>
        </IonContent>
      </IonModal>
    </IonPage>
  );
};

export default RecordConsole;
