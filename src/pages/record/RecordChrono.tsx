import {
  IonAlert,
  IonBackButton,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonDatetime,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonModal,
  IonPage,
  IonRow,
  IonSegment,
  IonSegmentButton,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToast,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import {
  addSharp,
  backspace,
  caretForwardOutline,
  checkmarkCircleOutline,
  closeCircle,
  closeCircleOutline,
  closeOutline,
  closeSharp,
  notificationsOutline,
  pause,
  pauseCircleOutline,
  pauseOutline,
  pauseSharp,
  play,
  playBackCircleOutline,
  playBackOutline,
  playCircleOutline,
  playForward,
  playOutline,
  powerOutline,
  squareOutline,
  terminalOutline,
} from "ionicons/icons";
import React, { useEffect, useRef, useState } from "react";
import "./RecordChrono.css";
import {
  CircularProgressbar,
  CircularProgressbarWithChildren,
  buildStyles,
} from "react-circular-progressbar";
import { OverlayEventDetail } from "@ionic/react/dist/types/components/react-component-lib/interfaces";

const Chronometer: React.FC = () => {
  const [isPaused, setIsPaused] = useState(false);

  const [isOpen, setIsOpen] = useState(false);
  const [isOpen2, setIsOpen2] = useState(false);

  const handleTogglePause = () => {
    setIsPaused(!isPaused);
  };

  //Alert
  const router = useIonRouter();
  const [showAlert, setShowAlert] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const handleDeleteConfirm = () => {
    // Handle delete logic here
    setShowAlert(false);
    setIsOpen2(false);
  };

  return (
    <IonPage>
      <IonHeader class="ion-no-border">
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/app/settings/summary">
              <IonIcon icon={backspace} title="none" />
            </IonBackButton>
          </IonButtons>
          <IonTitle></IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding ion-text-center">
        <IonLabel className="ion-text-center">
          <h1>
            Enregistrer l'activité avec <br /> un chronométre
          </h1>
        </IonLabel>
        <IonRow className=" ion-justify-content-center ion-margin-top ion-margin-bottom">
          <IonSegment>
            <IonSegmentButton
              value="segment-sport"
              className="ion-segment-button-sport"
            >
              Sport
            </IonSegmentButton>
            <IonSegmentButton
              value="segment-gaming"
              className="ion-segment-button-gaming"
            >
              Gaming
            </IonSegmentButton>
          </IonSegment>
        </IonRow>

        <IonRow class="ion-justify-content-center ion-margin-top">
          <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
            <IonItem>
              <IonSelect
                labelPlacement="floating"
                fill="solid"
                label="Pour qui ?"
                interface="popover"
                placeholder="Mohamed El Aaraj"
                multiple={true}
              >
                <IonSelectOption value="mohamedelaaraj">
                  Mohamed El Aaraj <span> (Père)</span>
                </IonSelectOption>
                <IonSelectOption value="thierry">
                  Thierry <span> (Enfant)</span>
                </IonSelectOption>
                <IonSelectOption value="luna">
                  Luna <span> (Enfant)</span>
                </IonSelectOption>
                <IonSelectOption value="alex">
                  Alex <span> (Enfant)</span>
                </IonSelectOption>
              </IonSelect>
            </IonItem>
          </IonCol>
        </IonRow>

        <IonRow class="ion-justify-content-center ion-margin-top">
          <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
            <IonButton
              shape="round"
              fill="solid"
              color={"secondary"}
              size="default"
              className="ion-margin-top ion-circle-button"
              onClick={() => setIsOpen(true)}
            >
              <IonIcon src={playOutline}></IonIcon>
            </IonButton>
          </IonCol>
        </IonRow>

        <IonModal isOpen={isOpen} trigger="open-modal">
          <IonHeader class="ion-no-border">
            <IonToolbar>
              <IonButtons slot="end">
                <IonButton onClick={() => setIsOpen(false)}>
                  <IonIcon icon={closeCircleOutline} />
                </IonButton>
              </IonButtons>

              <IonTitle></IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonContent className="ion-no-padding ion-text-center ">
            <IonRow class="ion-justify-content-center ion-margin-top">
              <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
                <IonLabel>
                  <h1>Ajouter une activité</h1>
                </IonLabel>
              </IonCol>
            </IonRow>

            <IonRow class="ion-justify-content-center ion-margin-top">
              <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
                <div className="ion-text-center centered-progress-container ">
                  <CircularProgressbarWithChildren
                    value={3}
                    text={"00:25:12"}
                    className=" ion-text-center circle-text-bold"
                    styles={buildStyles({
                      textSize: "18px",
                      // Colors
                      pathColor: `#467DF6`,
                      textColor: "#000000",
                      backgroundColor: "transparent",
                      pathTransitionDuration: 0.4,
                    })}
                  ></CircularProgressbarWithChildren>
                </div>
              </IonCol>
            </IonRow>

            <IonRow class="ion-justify-content-center ion-margin-top">
              <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
                {/* Additional content you want at the bottom */}
                <IonButton
                  shape="round"
                  fill="solid"
                  color={"primary"}
                  size="default"
                  className="ion-circle-button"
                  onClick={handleTogglePause}
                >
                  <IonIcon
                    src={isPaused ? pauseOutline : caretForwardOutline}
                  ></IonIcon>
                </IonButton>
              </IonCol>

              <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
                {/* Additional content you want at the bottom */}
                <IonButton
                  shape="round"
                  fill="solid"
                  color={"danger"}
                  size="default"
                  className="ion-circle-button"
                  onClick={() => setIsOpen2(true)}
                >
                  <IonIcon src={squareOutline}></IonIcon>
                </IonButton>
              </IonCol>
            </IonRow>
          </IonContent>
        </IonModal>

        <IonModal isOpen={isOpen2} trigger="open-modal-2">
          <IonHeader class="ion-no-border">
            <IonToolbar>
              <IonButtons slot="end">
                <IonButton onClick={() => setIsOpen2(false)}>
                  <IonIcon icon={closeCircleOutline} />
                </IonButton>
              </IonButtons>

              <IonTitle></IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonContent className="ion-no-padding ion-text-center ">
            <IonRow class="ion-justify-content-center ion-margin-top">
              <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
                <IonLabel>
                  <h1>Activité terminé</h1>
                </IonLabel>
                <IonIcon
                  className="ion-margin-top"
                  color={"success"}
                  src={checkmarkCircleOutline}
                ></IonIcon>
              </IonCol>
            </IonRow>

            <IonRow>
              <IonCol>
                <h3>Durée de l'activité</h3>
                <p>00:25:12</p>
              </IonCol>
            </IonRow>

            <IonRow class="ion-justify-content-center ion-margin-top ion-padding">
              <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
                {/* Additional content you want at the bottom */}
                <IonButton
                  shape="round"
                  fill="outline"
                  color={"medium"}
                  size="default"
                  expand="full"
                  className="ion-circle-button"
                  id="open-toast"
                >
                  Sauvgarder
                </IonButton>
                <IonToast
                  color={"success"}
                  trigger="open-toast"
                  message="Votre activité à été enregistré avec succées"
                  duration={5000}
                ></IonToast>
              </IonCol>

              <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
                {/* Additional content you want at the bottom */}
                <IonButton
                  shape="round"
                  expand="full"
                  fill="outline"
                  color={"danger"}
                  size="default"
                  className="ion-circle-button"
                  onClick={() => setShowAlert(true)}
                >
                  Supprimer
                </IonButton>
              </IonCol>
            </IonRow>
          </IonContent>
        </IonModal>

        <IonAlert
          isOpen={showAlert}
          onDidDismiss={() => setShowAlert(false)}
          header={"Confirmer la suppression"}
          message={"Êtes-vous sûr de vouloir supprimer cette activité?"}
          buttons={[
            {
              text: "Non",
              role: "cancel",
              handler: () => {
                // Handle 'No' button click
              },
            },
            {
              text: "Oui",
              handler: () => {
                // Handle 'Yes' button click
                handleDeleteConfirm();
              },
            },
          ]}
        />
      </IonContent>
    </IonPage>
  );
};

export default Chronometer;
