import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import {
  backspace,
  gift,
  giftOutline,
  notificationsOutline,
  person,
  timeOutline,
  trashBin,
} from "ionicons/icons";
import React from "react";

const Gift: React.FC = () => {
  return (
    <IonPage>
      <IonHeader class="ion-no-border">
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/app/settings/summary">
              <IonIcon icon={backspace} title="none" />
            </IonBackButton>
          </IonButtons>
          <IonButtons slot="end">
            <IonButton color="dark" id="open-modal" expand="block">
              <IonIcon
                slot="icon-only"
                ios={notificationsOutline}
                md={notificationsOutline}
              ></IonIcon>
            </IonButton>
          </IonButtons>

          <IonTitle>Cadeaux</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonCard id="home-card" className="ion-no-padding">
          <IonCardContent>
            <IonGrid>
              <IonRow>
                <IonCol>
                  <IonItem lines="none">
                    <IonIcon src={person}></IonIcon>
                    <IonLabel className="ion-margin">
                      <h2>Laura</h2>
                    </IonLabel>
                  </IonItem>
                  <IonItem lines="none">
                    <IonIcon src={timeOutline}></IonIcon>
                    <IonLabel className="ion-margin">
                      <h2>0h30</h2>
                    </IonLabel>
                  </IonItem>
                  <IonItem lines="none">
                    <p>Bravo pour la belle semaine !</p>
                  </IonItem>
                </IonCol>

                <IonCol size="auto">
                  <div style={{ width: "50px" }}>
                    <IonButton slot="end" color={"danger"}>
                      <IonIcon src={trashBin}></IonIcon>
                    </IonButton>
                    <IonButton>
                      <IonIcon src={gift}></IonIcon>
                    </IonButton>
                  </div>
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonCardContent>
        </IonCard>

        <IonCard id="home-card" className="ion-no-padding">
          <IonCardContent>
            <IonGrid>
              <IonRow>
                <IonCol>
                  <IonItem lines="none">
                    <IonIcon src={person}></IonIcon>
                    <IonLabel className="ion-margin">
                      <h2>Laura</h2>
                    </IonLabel>
                  </IonItem>
                  <IonItem lines="none">
                    <IonIcon src={timeOutline}></IonIcon>
                    <IonLabel className="ion-margin">
                      <h2>1h30</h2>
                    </IonLabel>
                  </IonItem>
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonCardContent>
        </IonCard>
      </IonContent>
    </IonPage>
  );
};

export default Gift;
