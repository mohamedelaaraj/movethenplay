import {
  IonAccordion,
  IonAccordionGroup,
  IonAvatar,
  IonBackButton,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCheckbox,
  IonCol,
  IonContent,
  IonDatetime,
  IonDatetimeButton,
  IonGrid,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonModal,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonRow,
  IonSegment,
  IonSegmentButton,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToast,
  IonToolbar,
} from "@ionic/react";
import { addSharp, backspace, notificationsOutline } from "ionicons/icons";
import React, { useEffect, useRef, useState } from "react";
import "./RecordManual.css";

const Activity: React.FC = () => {
  const accordionGroup = useRef<null | HTMLIonAccordionGroupElement>(null);
  useEffect(() => {
    if (!accordionGroup.current) {
      return;
    }

    accordionGroup.current.value = ["first", "third"];
  }, []);
  const [selectedSegment, setSelectedSegment] = useState<string>("segment1");

  const [selectedDate, setSelectedDate] = useState<string | undefined>(
    undefined
  );
  const [showToast, setShowToast] = useState(false);

  const handleDateChange = (event: CustomEvent) => {
    setSelectedDate(event.detail.value);
  };

  const handleSubmit = () => {
    if (!selectedDate) {
      setShowToast(true);
    } else {
      // Your form submission logic here
    }
  };
  return (
    <IonPage>
      <IonHeader class="ion-no-border">
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/app/settings/summary">
              <IonIcon icon={backspace} title="none" />
            </IonBackButton>
          </IonButtons>
          <IonTitle></IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonRow className=" ion-justify-content-center ion-margin-top ion-margin-bottom">
          <IonSegment className="">
            <IonSegmentButton
              value="segment-sport"
              className="ion-segment-button-sport "
            >
              Sport
            </IonSegmentButton>
            <IonSegmentButton
              value="segment-gaming"
              className="ion-segment-button-gaming "
            >
              Gaming
            </IonSegmentButton>
          </IonSegment>
        </IonRow>

        <IonRow class="ion-justify-content-center ion-margin-top">
          <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
            <IonItem>
              <IonSelect
                labelPlacement="floating"
                fill="solid"
                label="Pour qui ?"
                interface="popover"
                placeholder="Mohamed El Aaraj"
                multiple={true}
              >
                <IonSelectOption value="mohamedelaaraj">
                  Mohamed El Aaraj <span> (Père)</span>
                </IonSelectOption>
                <IonSelectOption value="thierry">
                  Thierry <span> (Enfant)</span>
                </IonSelectOption>
                <IonSelectOption value="luna">
                  Luna <span> (Enfant)</span>
                </IonSelectOption>
                <IonSelectOption value="alex">
                  Alex <span> (Enfant)</span>
                </IonSelectOption>
              </IonSelect>
            </IonItem>
          </IonCol>
        </IonRow>

        <IonRow class="ion-justify-content-center ion-margin-bottom">
          <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
            <IonItem>
              <IonSelect
                labelPlacement="floating"
                fill="solid"
                label="Description"
                interface="popover"
                multiple={true}
              >
                <IonSelectOption value="1">Choix 1</IonSelectOption>
                <IonSelectOption value="2">Choix 2</IonSelectOption>
                <IonSelectOption value="3">Choix 3</IonSelectOption>
                <IonSelectOption value="4">Choix 4</IonSelectOption>
                <IonSelectOption value="5">Choix 5</IonSelectOption>
                <IonSelectOption value="6">Choix 6</IonSelectOption>
                <IonSelectOption value="7">Choix 7</IonSelectOption>
                <IonSelectOption value="8">Choix 8</IonSelectOption>
              </IonSelect>
            </IonItem>
          </IonCol>
        </IonRow>

        <IonRow class="ion-justify-content-center  ">
          <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
            {/* Segment Buttons */}
            <IonSegment
              value={selectedSegment}
              onIonChange={(e) => setSelectedSegment(e.detail.value as string)}
            >
              <IonSegmentButton value="segment1">
                Date de début
              </IonSegmentButton>
              <IonSegmentButton value="segment2">Date de fin</IonSegmentButton>
            </IonSegment>

            {/* Content for Segment 1 */}
            {selectedSegment === "segment1" && (
              <IonDatetime
                onIonChange={handleDateChange}
                locale="fr"
                hourCycle="h12"
              ></IonDatetime>
            )}

            {/* Content for Segment 2 */}
            {selectedSegment === "segment2" && (
              <IonDatetime
                onIonChange={handleDateChange}
                locale="fr"
                hourCycle="h12"
              ></IonDatetime>
            )}
          </IonCol>
        </IonRow>

        <IonRow class="ion-justify-content-center">
          <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
            <IonButton
              shape="round"
              fill="outline"
              color={"primary"}
              expand="block"
              className="ion-margin-top"
              onClick={handleSubmit}
            >
              Ajouter l'activité
              <IonIcon src={addSharp}></IonIcon>
            </IonButton>
          </IonCol>
        </IonRow>

        <IonToast
          isOpen={showToast}
          onDidDismiss={() => setShowToast(false)}
          message="Please select a date and time."
          duration={2000}
        />
      </IonContent>
    </IonPage>
  );
};

export default Activity;
