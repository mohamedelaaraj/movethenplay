import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonInput,
  IonList,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
  useIonRouter,
} from "@ionic/react";
import React, { useState } from "react";
import logo from "/public/logo-black.png";

const Register: React.FC = () => {
  event?.preventDefault();
  console.log("doLogin");

  const [isTouched, setIsTouched] = useState(false);
  const [isValid, setIsValid] = useState<boolean>();

  const validateEmail = (email: string) => {
    return email.match(
      /^(?=.{1,254}$)(?=.{1,64}@)[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
    );
  };

  const validate = (ev: Event) => {
    const value = (ev.target as HTMLInputElement).value;

    setIsValid(undefined);

    if (value === "") return;

    validateEmail(value) !== null ? setIsValid(true) : setIsValid(false);
  };

  const markTouched = () => {
    setIsTouched(true);
  };

  const router = useIonRouter();
  const doRegister = (event: any) => {
    event.preventDefault();
    console.log("doRegister");
    router.goBack();
  };
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color={"secondary"}>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/" />
          </IonButtons>
          <IonTitle>Register</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent scrollY={false} className="ion-padding">
        <IonGrid fixed>
          <IonRow class="ion-justify-content-center">
            <IonCol size="12" sizeMd="8" sizeLg="6" sizeXl="4">
              <IonCard>
                <IonCardContent>
                  <div className="ion-text-center ion-margin">
                    <img src={logo} alt="" width={"50%"} />
                  </div>
                  <form>
                    <IonList>
                      <IonInput
                        className={`${isValid && "ion-valid"} ${
                          isValid === false && "ion-invalid"
                        } ${isTouched && "ion-touched"}`}
                        type="email"
                        fill="outline"
                        label="Email"
                        labelPlacement="floating"
                        clearInput={true}
                        errorText="Invalid email"
                        onIonInput={(event) => validate(event)}
                        onIonBlur={() => markTouched()}
                      />

                      <IonInput
                        className={`ion-margin-top ${isValid && "ion-valid"} ${
                          isValid === false && "ion-invalid"
                        } ${isTouched && "ion-touched"}`}
                        fill="outline"
                        type="number"
                        label="Téléphone portable"
                        labelPlacement="floating"
                        clearInput={true}
                      />

                      <IonInput
                        label="Mot de passe"
                        labelPlacement="floating"
                        fill="outline"
                        className="ion-margin-top"
                        type="password"
                      ></IonInput>

                      <IonInput
                        label="Confirmez votre mot de passe"
                        labelPlacement="floating"
                        fill="outline"
                        className="ion-margin-top"
                        type="password"
                      ></IonInput>
                    </IonList>

                    <IonButton
                      shape="round"
                      type="submit"
                      expand="block"
                      className="ion-margin-top"
                    >
                      Ouvrir mon compte
                    </IonButton>
                    <IonButton
                      shape="round"
                      fill="outline"
                      routerLink="/login"
                      color={"secondary"}
                      type="button"
                      expand="block"
                      className="ion-margin-top"
                    >
                      J'ai déja un compte
                    </IonButton>
                  </form>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Register;
