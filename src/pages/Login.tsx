import { IonButton, IonCard, IonCardContent, IonCol, IonContent, IonFooter, IonGrid, IonHeader, IonInput, IonItem, IonList, IonPage, IonRippleEffect, IonRow, IonTitle, IonToolbar, useIonLoading, useIonRouter } from '@ionic/react';
import React, { useEffect, useState } from 'react';
import logo from "/public/logo-black.png"
import Intro from '../components/Intro';
import { Preferences } from '@capacitor/preferences';

const INTRO_KEY = 'intro-seen'
const Login: React.FC = () => {

  const router = useIonRouter();

  const [introSeen, setIntroSeen] = useState(true);
  const [present, dismiss] = useIonLoading();

  useEffect(()=>{
    const checkStorage = async ()=> {
      const seen = await Preferences.get({key: INTRO_KEY});
      setIntroSeen(seen.value === 'true');
    }
    checkStorage();

  },[]);

  const doLogin = async (event : any) =>{
    event.preventDefault();
    await present('Logging in ...');
    setTimeout(()=>{
      dismiss();
      router.push('/app','forward');
    },1000);
    // console.log('dologin');
    
  };

  const finishIntro = async()=>{
    setIntroSeen(true);
    Preferences.set({key:INTRO_KEY, value: 'true'});
  }

  
    return (

      <>
      {!introSeen ? (
        <Intro onFinish={finishIntro}/>
      ) : (
      
        <IonPage>
          <IonHeader>
            <IonToolbar color={'secondary'}>
              <IonTitle>Login</IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent scrollY={false} className="ion-padding">

          <IonGrid fixed>
            

            <IonRow class='ion-justify-content-center'>
              <IonCol size='12' sizeMd='8' sizeLg='6' sizeXl='4'>
              <IonCard>
          
          <IonCardContent>
          <div className='ion-text-center ion-margin'>
                            <img src={logo} alt="" width={'50%'} />
                        </div>
            <form onSubmit={doLogin}>
          <IonInput 
            label='Email'
            labelPlacement='floating'
            fill='outline'
            className='ion-margin-top'
            type='email'>
          </IonInput>
  
          <IonInput
            label="Mot de passe"
            labelPlacement="floating"
            fill="outline"
            className='ion-margin-top'
            type="password"
          ></IonInput>
  
          <IonButton 
            type="submit"
            shape="round" 
            expand="block" 
            className='ion-margin-top'>Se connecter</IonButton>
          <IonButton 
            shape="round" 
            fill='outline' 
            routerLink='/register' 
            color={"secondary"} 
            expand="block" 
            className='ion-margin-top'>Crée un compte</IonButton>
  
          </form>
          </IonCardContent>
          </IonCard>

              </IonCol>
            </IonRow>
          </IonGrid>
              
              
       
            
        </IonContent>
            
        </IonPage>
      )}

        </>
    );
};

export default Login;