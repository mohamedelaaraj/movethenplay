import {
  IonBackButton,
  IonBadge,
  IonButtons,
  IonContent,
  IonFab,
  IonFabButton,
  IonFabList,
  IonHeader,
  IonIcon,
  IonLabel,
  IonMenuButton,
  IonPage,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  IonText,
  IonTitle,
  IonToolbar,
  useIonViewDidEnter,
} from "@ionic/react";
import {
  add,
  addCircle,
  addOutline,
  addSharp,
  chevronUpCircle,
  colorPalette,
  globe,
  home,
  person,
  personAdd,
  personAddSharp,
  personOutline,
  personSharp,
  recording,
  triangle,
  document,
  settingsOutline,
  menuOutline,
  flashOffOutline,
  flashOutline,
  peopleOutline,
} from "ionicons/icons";
import React, { useState } from "react";
import { Redirect, Route } from "react-router";
import Tab1 from "./Tabs/Summary";
import Tab2 from "./Tabs/Tab2";
import Tab3 from "./record/RecordManual";
import Profile from "./Profile";
import Tab5 from "./Tabs/Tab5";
import Tab4 from "./Tabs/Tab4";
import Summary from "./Tabs/Summary";
import Activity from "./record/RecordManual";
import "./Tabs/Tabs.css";
import Gift from "./record/AddGift";
import Repport from "./Repport";
import Avatar from "./Avatar";
import PrimaryFabButton from "./Elements/PrimaryFabButton";
import Chronometer from "./record/RecordChrono";
import RecordManual from "./record/RecordManual";
import RecordChrono from "./record/RecordChrono";
import RecordConsole from "./record/RecordConsole";
import AddGift from "./record/AddGift";

const Settings: React.FC = () => {
  const [activeTab, setActiveTab] = useState("/app/settings/summary");
  useIonViewDidEnter(() => {
    setActiveTab(window.location.pathname);
  });

  return (
    <>
      <IonTabs>
        <IonTabBar slot="bottom">
          <IonTabButton tab="summary" href="/app/settings/summary">
            <IonIcon icon={home} />
            <IonLabel>Accueil</IonLabel>
            {activeTab === "/app/settings/summary" && (
              <IonBadge color="danger"></IonBadge>
            )}
          </IonTabButton>

          <IonTabButton tab="tab2" href="/app/settings/tab2">
            <IonIcon icon={personOutline} />
            <IonLabel>Profile</IonLabel>
          </IonTabButton>

          <IonTabButton></IonTabButton>

          <IonTabButton tab="tab4" href="/app/settings/tab4">
            <IonIcon icon={flashOutline}></IonIcon>
            <IonLabel>Activités</IonLabel>
          </IonTabButton>

          <IonTabButton tab="tab5" href="/app/settings/tab5">
            <IonIcon icon={peopleOutline} />
            <IonLabel>Famille</IonLabel>
          </IonTabButton>
        </IonTabBar>

        <IonRouterOutlet>
          <Route path="/app/settings/summary" component={Summary} />
          <Route path="/app/settings/record-manual" component={RecordManual} />
          <Route path="/app/settings/tab3" component={Tab3} />
          <Route path="/app/settings/tab4" component={Tab4} />
          <Route path="/app/settings/tab5" component={Tab5} />
          <Route path="/app/settings/add-gift" component={AddGift} />
          <Route path="/app/settings/repport" component={Repport} />
          <Route path="/app/settings/avatar" component={Avatar} />
          <Route
            path="/app/settings/record-chronometer"
            component={RecordChrono}
          />
          <Route
            path="/app/settings/record-console"
            component={RecordConsole}
          />

          <Route exact path="/app/tabs">
            <Redirect to="/app/tab1" />
          </Route>
        </IonRouterOutlet>
      </IonTabs>

      <PrimaryFabButton />
    </>
  );
};

export default Settings;
