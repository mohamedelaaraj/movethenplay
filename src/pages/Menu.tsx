import {
  IonButton,
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonMenu,
  IonMenuToggle,
  IonPage,
  IonRouterOutlet,
  IonSplitPane,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import Home from "./Home";
import { Redirect, Route } from "react-router";
import Settings from "./Settings";
import {
  add,
  addOutline,
  homeOutline,
  logOutOutline,
  settingsOutline,
  timerOutline,
} from "ionicons/icons";
import Family from "./Family";
import "./Menu.css";

const Menu: React.FC = () => {
  const paths = [
    { name: "Accueil", url: "/app/settings/summary", icon: homeOutline },
    { name: "Settings", url: "/app/settings/tab2", icon: settingsOutline },
    { name: "Activités", url: "/app/settings/activity", icon: homeOutline },
    { name: "Famille", url: "/app/family", icon: homeOutline },
    { name: "Record", url: "/app/record", icon: timerOutline },
    { name: "Test", url: "/app/tabs", icon: timerOutline },
    { name: "Se deconnecté", url: "/app/logout", icon: logOutOutline },
  ];

  return (
    <IonPage>
      <IonSplitPane when="ms" contentId="main">
        <IonMenu contentId="main" className="navigation">
          <IonContent>
            <div className="menu-header-bg"></div>
            <div className="header-content">
              <img
                className="img-profile-menu"
                src="../public/med.png"
                alt="profile"
              />
              <IonLabel>
                <h2 className="h2-profile-menu">P.MOHAMED EL AARAJ</h2>
                <p className="p-profile-menu">Pére</p>
              </IonLabel>
            </div>

            <div className="action-button ion-justify-content">
              <IonButton
                className="btn-profile-menu"
                color={"primary"}
                shape="round"
                expand="block"
              >
                <IonIcon slot="start" icon={addOutline}></IonIcon>
                Ajouter une activité
              </IonButton>
            </div>
            {paths.map((item, index) => (
              <IonMenuToggle
                key={index}
                autoHide={false}
                className="menu-items"
              >
                <IonItem
                  className="ion-item-menu"
                  lines="none"
                  detail={false}
                  routerLink={item.url}
                  routerDirection="none"
                >
                  <IonIcon
                    className="ion-icon-menu"
                    slot="start"
                    icon={item.icon}
                  />
                  {item.name}
                </IonItem>
              </IonMenuToggle>
            ))}
          </IonContent>
        </IonMenu>

        <IonRouterOutlet id="main">
          <Route exact path="/app/home" component={Home} />
          <Route path="/app/settings" component={Settings} />
          <Route exact path="/app">
            <Redirect to="/app/settings/summary" />
          </Route>
        </IonRouterOutlet>
      </IonSplitPane>
    </IonPage>
  );
};

export default Menu;
