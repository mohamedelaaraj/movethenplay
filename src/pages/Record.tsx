import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';

const Record: React.FC = () => {

    return (
        <>
    <IonHeader>
      <IonToolbar>
        <IonTitle>Record</IonTitle>
      </IonToolbar>
    </IonHeader>
    <IonContent>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
        }}
      >
        Record
      </div>
    </IonContent>
  </>
    );
};

export default Record;