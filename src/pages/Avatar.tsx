import {
  IonAvatar,
  IonBackButton,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonIcon,
  IonImg,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { backspace, notificationsOutline } from "ionicons/icons";
import React from "react";
import "./Avatar.css";

const Avatar: React.FC = () => {
  return (
    <IonPage>
      <IonHeader class="ion-no-border">
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/app/settings/summary">
              <IonIcon icon={backspace} title="none" />
            </IonBackButton>
          </IonButtons>
          <IonButtons slot="end">
            <IonButton color="dark" id="open-modal" expand="block">
              <IonIcon
                slot="icon-only"
                ios={notificationsOutline}
                md={notificationsOutline}
              ></IonIcon>
            </IonButton>
          </IonButtons>

          <IonTitle>Avatar</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent class="mtp-main-content" fullscreen>
        <div className="mtp-page-container">
          <div
            style={{
              display: "flex",
              alignItems: "center",
              marginBottom: "10px",
            }}
          >
            <h1 className="ion-margin-start">Choisir votre avatar</h1>
          </div>
          <div className="mtp-back-progress-bar-header"></div>
          <div className="w100" style={{ textAlign: "left" }}>
            <div style={{ marginBottom: 40 }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  flexWrap: "wrap",
                }}
              >
                <div>
                  <IonImg src={""} />
                  <IonImg src={""} />
                </div>
              </div>
            </div>
            <IonButton color="dark" expand="full" shape="round"></IonButton>
          </div>
          <div></div>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Avatar;
