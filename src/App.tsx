import { Redirect, Route } from "react-router-dom";
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupIonicReact,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Profile from "./pages/Profile";
import Record from "./pages/Record";
import Family from "./pages/Family";

import {
  playCircle,
  radio,
  library,
  search,
  home,
  personOutline,
  addCircle,
  menu,
  peopleOutline,
} from "ionicons/icons";
import Home from "./pages/Home";
import Menu from "./pages/Menu";
import Settings from "./pages/Settings";
import Gift from "./pages/record/AddGift";

setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route exact path="/">
          <Login />
        </Route>

        <Route component={Menu} path="/app" />
        <Route component={Register} path="/register" exact />
        <Route component={Login} path="/login" exact />
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
